<%--
 =================================================================
  Licensed Materials - Property of IBM

  WebSphere Commerce

  (C) Copyright IBM Corp. 2011, 2014 All Rights Reserved.

  US Government Users Restricted Rights - Use, duplication or
  disclosure restricted by GSA ADP Schedule Contract with
  IBM Corp.
 =================================================================
--%>

<!doctype HTML>

<!-- BEGIN TopCategoriesDisplay.jsp -->

<%@include file="../../../Common/EnvironmentSetup.jspf" %>
<%@include file="../../../Common/nocache.jspf" %>
<%@ taglib uri="http://commerce.ibm.com/coremetrics"  prefix="cm" %>
<%@ taglib uri="http://commerce.ibm.com/pagelayout" prefix="wcpgl" %>

<wcf:rest var="getPageResponse" url="store/{storeId}/page/name/{name}">
	<wcf:var name="storeId" value="${storeId}" encode="true"/>
	<wcf:var name="name" value="HomePage" encode="true"/>
	<wcf:param name="langId" value="${langId}"/>
	<wcf:param name="profileName" value="IBM_Store_Details"/>
</wcf:rest>
<c:set var="page" value="${getPageResponse.resultList[0]}"/>

<wcf:rest var="getPageDesignResponse" url="store/{storeId}/page_design">
	<wcf:var name="storeId" value="${storeId}" encode="true"/>
	<wcf:param name="catalogId" value="${catalogId}"/>
	<wcf:param name="langId" value="${langId}"/>
	<wcf:param name="q" value="byObjectIdentifier"/>
	<wcf:param name="objectIdentifier" value="${page.pageId}"/>
	<wcf:param name="deviceClass" value="${deviceClass}"/>
	<wcf:param name="pageGroup" value="Content"/>
</wcf:rest>
<c:set var="pageDesign" value="${getPageDesignResponse.resultList[0]}" scope="request"/>
<c:set var="PAGE_DESIGN_DETAILS_JSON_VAR" value="pageDesign" scope="request"/>

<c:set var="pageTitle" value="${page.title}" />
<c:set var="metaDescription" value="${page.metaDescription}" />
<c:set var="metaKeyword" value="${page.metaKeyword}" />
<c:set var="fullImageAltDescription" value="${page.fullImageAltDescription}" scope="request" />
<c:set var="pageCategory" value="Browse" scope="request"/>

<html xmlns:wairole="http://www.w3.org/2005/01/wai-rdf/GUIRoleTaxonomy#"
<flow:ifEnabled feature="FacebookIntegration">
	<%-- Facebook requires this to work in IE browsers --%>
	xmlns:fb="http://www.facebook.com/2008/fbml" 
	xmlns:og="http://opengraphprotocol.org/schema/"
</flow:ifEnabled>
xmlns:waistate="http://www.w3.org/2005/07/aaa" lang="${shortLocale}" xml:lang="${shortLocale}">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><c:out value="${pageTitle}"/></title>
		<meta name="description" content="<c:out value="${metaDescription}"/>"/>
		<meta name="keywords" content="<c:out value="${metaKeyword}"/>"/>
		<meta name="pageIdentifier" content="HomePage"/>
		<meta name="pageId" content="<c:out value="${page.pageId}"/>"/>
		<meta name="pageGroup" content="content"/>	
	    <link rel="canonical" href="<c:out value="${env_TopCategoriesDisplayURL}"/>" />
		
		<!--Main Stylesheet for browser -->
		<link rel="stylesheet" href="${jspStoreImgDir}${env_vfileStylesheet}" type="text/css" media="screen"/>
		<!-- Style sheet for print -->
		<link rel="stylesheet" href="${jspStoreImgDir}${env_vfileStylesheetprint}" type="text/css" media="print"/>
		
		<!-- Include script files -->
		<%@include file="../../../Common/CommonJSToInclude.jspf" %>
		
		<%-- 
		<script type="text/javascript">
			dojo.addOnLoad(function() { 
				shoppingActionsJS.setCommonParameters('<c:out value="${langId}"/>','<c:out value="${storeId}" />','<c:out value="${catalogId}" />','<c:out value="${userType}" />','<c:out value="${env_CurrencySymbolToFormat}" />');
				shoppingActionsServicesDeclarationJS.setCommonParameters('<c:out value="${langId}"/>','<c:out value="${storeId}" />','<c:out value="${catalogId}" />');
				});
			<c:if test="${!empty requestScope.deleteCartCookie && requestScope.deleteCartCookie[0]}">					
				document.cookie = "WC_DeleteCartCookie_${requestScope.storeId}=true;path=/";				
			</c:if>
		</script>
		--%>
		<wcpgl:jsInclude/>
		
		<flow:ifEnabled feature="FacebookIntegration">
			<%@include file="../../../Common/JSTLEnvironmentSetupExtForFacebook.jspf" %>
			<%--Facebook Open Graph tags that are required  --%>
			<meta property="og:title" content="<c:out value="${pageTitle}"/>" /> 			
			<meta property="og:image" content="<c:out value="${schemeToUse}://${request.serverName}${portUsed}${jspStoreImgDir}images/logo.png"/>" />
			<meta property="og:url" content="<c:out value="${env_TopCategoriesDisplayURL}"/>"/>
			<meta property="og:type" content="website"/>
			<meta property="fb:app_id" name="fb_app_id" content="<c:out value="${facebookAppId}"/>"/>
			<meta property="og:description" content="${page.metaDescription}" />
		</flow:ifEnabled>
	</head>
	
	
	<body ng-app="scopeExample">
	
		<div id="page">
			
			<div id="homepage">
				
				<div class="body">
	            <div class="Example">
					 <p>This is your first angular expression</p>

					
					<div ng-controller="MyController">
					  Your name:
					    <input type="text" ng-model="username">
					    <button ng-click='sayHello()'>greet</button>
					  <hr>
					  {{greeting}}
					</div>
				</div>	
					<div class="section-1">
						<div id="headerWrapper">
						<c:set var="overrideLazyLoadDepartmentsList" value="true" scope="request"/>
						<%out.flush();%>
						<c:import url = "${env_jspStoreDir}Widgets/Header/Header.jsp">
							<c:param name="overrideLazyLoadDepartmentsList" value="${overrideLazyLoadDepartmentsList}" />
						</c:import>
						<%out.flush();%>
					</div>
						
						<div class="home-carousel">
							<div class="item">
								<a href="#">
									<img src="${jspStoreImgDir}images/espots/home-carousel-1.jpg"/>
									<div class="desc">
										<h1>JACKETS</h1>
										<div class="offer-t1">20% OFF</div>
										<a href="#"><div class="button primary">Shop Now</div></a>
									</div>
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="${jspStoreImgDir}images/espots/home-carousel-2.jpg"/>
									<div class="desc">
										<h1>City Style</h1>
										<a href="#"><div class="button primary">Shop Now</div></a>
										<div class="offer-t2">70% OFF</div>
									</div>
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="${jspStoreImgDir}images/espots/home-carousel-3.jpg"/>
									<div class="desc">
										<h1>Shirts</h1>
										<div class="offer-t3">10% OFF</div>
										<a href="#"><div class="button primary">Shop Now</div></a>
									</div>
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="${jspStoreImgDir}images/espots/home-carousel-4.jpg"/>
									<div class="desc">
										<h1>Handbags</h1>
										<div class="offer-t1">20% OFF</div>
										<a href="#"><div class="button primary">Shop Now</div></a>
									</div>
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="${jspStoreImgDir}images/espots/home-carousel-5.jpg"/>
									<div class="desc">
										<div class="offer-t4">20% OFF</div>
										<h1>Shoes</h1>
										<a href="#"><div class="button primary">Shop Now</div></a>
									</div>
								</a>
							</div>
							<div class="item">
								<a href="#">
									<img src="${jspStoreImgDir}images/espots/home-carousel-6.jpg"/>
									<div class="desc">
										<h1>Designer Collections</h1>
										<div class="offer-t1">20% OFF</div>
										<a href="#"><div class="button primary">Shop Now</div></a>
									</div>
								</a>
							</div>
							
						</div>
					
					</div>

				</div>
			</div>
			
			<div id="footer">
					
			</div>
		</div>
	</body>
	
	
<wcpgl:pageLayoutCache pageLayoutId="${pageDesign.layoutId}" pageId="${page.pageId}"/>
<!-- END TopCategoriesDisplay.jsp -->		
</html>
