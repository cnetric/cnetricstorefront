define(["jquery", "owl.carousel.min", "jquery.slimscroll"], function($) {
    
    $(function() {
        //console.debug("owl carousel is loaded");
		
				 
		$('.slideout-menu-toggle').on('click', function(event){
			event.preventDefault();
			// create menu variables
			var slideoutMenu = $('.slideout-menu');
			var slideoutMenuWidth = $('.slideout-menu').width();
			
			// toggle open class
			slideoutMenu.toggleClass("open");
			
			// slide menu
			if (slideoutMenu.hasClass("open")) {
				slideoutMenu.animate({
					left: "0px"
				});	
			} else {
				slideoutMenu.animate({
					left: -slideoutMenuWidth
				}, 250);	
			}
		});
			
		
		
		$('.home-carousel').owlCarousel({
			loop:true,
			margin:10,
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:true
				},
				600:{
					items:2,
					nav:false
				},
				1000:{
					items:3,
					nav:true,
					loop:false
				}
			}
		})
		
    }),
    $(function(){
   	 $('.scroll').slimScroll({
   		    size: '10px',
   		    color:'#fff',
   		    height: $( window ).height()
   		  });
   });
  
    
});

