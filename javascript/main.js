// Place third party dependencies in the lib folder
//
// Configure loading modules from the lib directory,
// except 'app' ones, 
requirejs.config({
    "baseUrl": "/wcsstore/CnetricStorefront/javascript/lib",
    "paths": {
      "common": "/wcsstore/CnetricStorefront/javascript/common",
	  "pages": "/wcsstore/CnetricStorefront/javascript/pages",
      'angular' : '/wcsstore/CnetricStorefront/javascript/angular/angular',
      'ngResource': '/wcsstore/CnetricStorefront/javascript/angular/angular-resource',
      'ngCookies': '/wcsstore/CnetricStorefront/javascript/angular/angular-cookies',
     
    },
    "shim": {
        "owl.carousel.min": ["jquery"],
        "ngResource": {deps: ['angular'],exports: 'angular'},
	    "ngCookies": {deps: ['angular'],exports: 'angular'},
	    "angular": {exports : 'angular'}

    }
});

// Load the main app module to start the app
requirejs(["common/common"]);
requirejs(["pages/homepage"]);
requirejs(["pages/search"]);
requirejs(["pages/minishopcart"]);
requirejs(["pages/signin"]);
requirejs(["pages/app"]);

